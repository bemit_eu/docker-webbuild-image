# WebBuild: Base Image for PHP and NodeJS based web application

Image includes different basic utils and pre-installed packages.

Features

- Ubuntu 18.04
- Basic Tools: curl, wget, git, lftp, unzip, zip
- Image Libraries imagemagick
- handbrake CLI
- NodeJS + NPM with nvm
    - `8.9.4`, `8.14.1`, `10.14.2` (default)
- Yarn
- PHP 7.3
    - with a lot of modules
- composer
- build tool util scripts
    - publish to npm
    - switch Node version
    - bitbucket save to downloads

# Build Image

```bash
docker build -t bemiteu/webbuild .
docker image save -o img.tar bemiteu/webbuild

# Start Container and open bash
docker run --name webbuild --rm -i -t bemiteu/webbuild bash
```