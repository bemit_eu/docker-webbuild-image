#!/usr/bin/env bash

npm version ${BITBUCKET_TAG}
echo '//registry.npmjs.org/:_authToken=${NPM_TOKEN}' > .npmrc
npm publish --access public