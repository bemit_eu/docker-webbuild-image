#!/usr/bin/env bash

NO_FORMAT="\e[39m"
C_CYAN1="\e[36m"

echo -e "${NO_FORMAT}nvm switch Node version: ${C_CYAN1}10.14.2${NO_FORMAT}"
. $HOME/.nvm/nvm.sh && nvm use 10.14.2