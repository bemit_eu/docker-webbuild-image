#!/usr/bin/env bash

cp $1 ${BITBUCKET_BRANCH}_build-${BITBUCKET_BUILD_NUMBER}.zip

curl -f -X POST --user "${BB_AUTH_STRING}" "https://api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"${BITBUCKET_BRANCH}_build-${BITBUCKET_BUILD_NUMBER}.zip"